<?php
	$target = 'icon_type';
	$name = 'type_name';
	$sign = 'sign';

	if(!is_null($_GET[$target])){
		$target=$_GET[$target];
		//echo $target . '</br>';
	}
	if(!is_null($_GET[$name])){
		$name=$_GET[$name];
		//echo $name . '</br>';
	}
	if(!is_null($_GET[$sign])){
		$sign=$_GET[$sign];
		//echo $sign . '</br>';
	}

	include_once "database.php";
	try{
		$conn = new PDO("mysql:host=$db_host;dbname=$db_database", $db_user, $db_psd);
		$conn->exec("set names utf8");
		$conn->setAttribute( PDO :: ATTR_ERRMODE , PDO :: ERRMODE_EXCEPTION );
		echo '<script type="text/javascript">';
		echo 'console.info("Connection SQL Successfully")';
		echo '</script>';
	}catch(PDOException $e){
		echo '<script type="text/javascript">';
		echo 'console.error("Connection SQL Failed: '.$e->getMessage().'")';
		echo '</script>';
	}


	switch ($sign) {
		case 'change':
			$sql="UPDATE $db_type_table SET `type_name` = :type_name WHERE `type_id` = :type_id";
			$pdo = $conn->prepare($sql);
			$pdo->bindParam ( ':type_id' , $target );
			$pdo->bindParam ( ':type_name' , $name );
			$pdo ->execute();
			echo "更新成功!";
			break;

		case 'add':
			$sql="INSERT INTO $db_type_table (`type_name`) VALUE(:type_name)";
			$pdo = $conn->prepare($sql);
			$pdo->bindParam ( ':type_name' , $name );
			$pdo ->execute();
			echo "新增成功";
			break;

		case 'delete':
			$sql="DELETE FROM $db_type_table WHERE `type_id` = :type_id";
			$pdo = $conn->prepare($sql);
			$pdo->bindParam ( ':type_id' , $target );
			$pdo ->execute();
			echo "刪除成功!";
			break;
			break;

		default:
			echo "Error!";
			break;
	}
?>