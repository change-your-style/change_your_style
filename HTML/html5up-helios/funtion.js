function upper_hide(){
	var status = document.getElementById('upper_img').hidden;
	if(status){
		document.getElementById('upper_img').hidden=false;
		document.getElementById('hide_upper_canvas').innerHTML="隱藏";
		document.getElementById('request_img').style.height='47%';
	}else{
		document.getElementById('upper_img').hidden=true;
		document.getElementById('hide_upper_canvas').innerHTML="顯示";
		document.getElementById('request_img').style.height='80%';
	}
}

function draw_hide(){
	var status = document.getElementById('paintbox').hidden;
	if(status){
		document.getElementById('paintbox').hidden=false;
		document.getElementById('stylelist').hidden=true;
		document.getElementById('paint').innerHTML="繪圖工具";
		document.getElementById('paintbox').style.height='50%';
	}else{
		document.getElementById('paintbox').hidden=true;
		document.getElementById('paint').innerHTML="繪圖工具";
		document.getElementById('paintbox').style.height='0%';
	}
}

function style_hide(){
	var status = document.getElementById('stylelist').hidden;
	if(status){
		document.getElementById('stylelist').hidden=false;
		document.getElementById('paintbox').hidden=true;
		document.getElementById('style').innerHTML="特效";
		document.getElementById('stylelist').style.height='50%';
	}else{
		document.getElementById('stylelist').hidden=true;
		document.getElementById('style').innerHTML="特效";
		document.getElementById('stylelist').style.height='0%';
	}
}

