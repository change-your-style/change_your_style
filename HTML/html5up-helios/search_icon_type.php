<?php
	include_once "database.php";
	try{
		$conn = new PDO("mysql:host=$db_host;dbname=$db_database", $db_user, $db_psd);
		$conn->exec("set names utf8");
		$conn->setAttribute( PDO :: ATTR_ERRMODE , PDO :: ERRMODE_EXCEPTION );
		echo '<script type="text/javascript">';
		echo 'console.info("Connection SQL Successfully")';
		echo '</script>';
	}catch(PDOException $e){
		echo '<script type="text/javascript">';
		echo 'console.error("Connection SQL Failed: '.$e->getMessage().'")';
		echo '</script>';
	}

	$sql="SELECT * FROM $db_type_table ORDER BY 'type_id' ASC;";
	//echo $sql . "</br>";
	$pdo = $conn->prepare($sql);
	$pdo ->execute();
	$row = $pdo->fetchAll(PDO::FETCH_ASSOC);
	//var_dump($row);

	foreach ($row as $key => $value) {
		echo '<option value="'.$row[$key]['type_id'].'">'. $row[$key]['type_name'] .'</option>';
	}

?>