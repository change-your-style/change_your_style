<?php
	include_once "database.php";
	try{
		$conn = new PDO("mysql:host=$db_host;dbname=$db_database", $db_user, $db_psd);
		$conn->exec("set names utf8");
		$conn->setAttribute( PDO :: ATTR_ERRMODE , PDO :: ERRMODE_EXCEPTION );
		echo '<script type="text/javascript">';
		echo 'console.info("Connection SQL Successfully")';
		echo '</script>';
	}catch(PDOException $e){
		echo '<script type="text/javascript">';
		echo 'console.error("Connection SQL Failed: '.$e->getMessage().'")';
		echo '</script>';
	}

	$sql="SELECT * FROM $db_icon_table ORDER BY 'ic_name' ASC;";
	//echo $sql . "</br>";
	$pdo = $conn->prepare($sql);
	//echo $_GET['type'] . "</br>";
	//$pdo->bindParam ( ':type' , $_GET['type'] );
	$pdo ->execute();
	$row = $pdo->fetchAll(PDO::FETCH_ASSOC);
	//var_dump($row);

	if($_GET['type']==0){
		echo '<p>請先選擇Icon類別</p>';
	}else{
		foreach ($row as $key => $value) {
			if($row[$key]['ic_type']==$_REQUEST['type']){
				echo '<div style="position:relative;top:0;right:0;z-index:0;float:left;">';
				echo '	<label for="option">';
				echo '		<div id="'. $row[$key]['ic_id'] . $row[$key]['ic_name'] .'" draggable="true" ondragstart="">';
				echo '			<img title="'. $row[$key]['ic_name'] .'" class="image check" src="../../'. $row[$key]['ic_path'] .'" style="width:100px;height:100px;">';
				echo '		</div>';
				echo '	</label>';
				echo '	<input style="position:absolute;top:0;left:0;z-index:10" id="icon_'. $row[$key]['ic_id'] .'" type="radio" name="option" value="../../'. str_replace('\\' , '/' , $row[$key]['ic_path']) .'" onclick="load_icon_info(this);">';
				echo '</div>';
			}
		}
	}
?>