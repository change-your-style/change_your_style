function sobel() {			//炭筆
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, src, cv.COLOR_RGB2GRAY, 0);
	// You can try more different parameters
	cv.Laplacian(src, dst, cv.CV_8U, 1, 20, 0, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();

}

function filter() {			//曝光
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let M = cv.Mat.eye(3, 3, cv.CV_32FC1);
	let anchor = new cv.Point(-1, -1);
	cv.filter2D(src, dst, cv.CV_8U, M, anchor, 0, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function erosion() {		//侵蝕
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let M = cv.Mat.ones(10, 10, cv.CV_8U);
	let anchor = new cv.Point(-1, -1);
	// You can try more different parameters
	cv.dilate(src, dst, M, anchor, 2, cv.BORDER_CONSTANT, cv.morphologyDefaultBorderValue());
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function BLhat() {			//反相
	let src = cv.imread('imageSrc');
	cv.cvtColor(src, src, cv.COLOR_RGBA2RGB);
	let dst = new cv.Mat();
	let M = cv.Mat.ones(53, 53, cv.CV_8U);
	cv.morphologyEx(src, dst, cv.MORPH_BLACKHAT, M);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function code() {			//碼
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let low = new cv.Mat(src.rows, src.cols, src.type(), [0, 0, 0, 0]);
	let high = new cv.Mat(src.rows, src.cols, src.type(), [150, 150, 150, 255]);
	cv.inRange(src, low, high, dst);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); low.delete(); high.delete();
}

function gray() {			//灰階
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, dst, cv.COLOR_RGBA2GRAY, 0);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function unclear() {		//模糊
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let ksize = new cv.Size(3, 10);
	let anchor = new cv.Point(-1, -1);
	// You can try more different parameters
	cv.blur(src, dst, ksize, anchor, cv.BORDER_DEFAULT);
	// cv.boxFilter(src, dst, -1, ksize, anchor, true, cv.BORDER_DEFAULT)
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function smooth() {			//平滑
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, src, cv.COLOR_RGBA2RGB, 0);
	cv.bilateralFilter(src, dst, 9, 200, 200, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function face(){
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	// You can try more different parameters
	let rect = new cv.Rect(100, 100, 200, 200);
	dst = src.roi(rect);
	//let s = new cv.Scalar(255,255,255,0);
 	//cv.copyMakeBorder(src,dst,10,10,10,10,cv.BORDER_CONSTANT,s);
	cv.imshow('canvasOutput', dst);
	src.delete();
	dst.delete();
}

