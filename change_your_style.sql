-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2021-01-18 08:37:49
-- 伺服器版本： 10.4.14-MariaDB
-- PHP 版本： 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `change_your_style`
--

-- --------------------------------------------------------

--
-- 資料表結構 `icon`
--

CREATE TABLE `icon` (
  `ic_id` int(11) NOT NULL,
  `ic_name` varchar(32) NOT NULL,
  `ic_path` text NOT NULL,
  `ic_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='icon存放位置';

--
-- 傾印資料表的資料 `icon`
--

INSERT INTO `icon` (`ic_id`, `ic_name`, `ic_path`, `ic_type`) VALUES
(1, '001-rainy', 'icon\\大自然可愛圖\\001-rainy.png', 1),
(52, '002-hedgehog', 'icon\\大自然可愛圖\\002-hedgehog.png', 1),
(53, '003-pumpkin', 'icon\\大自然可愛圖\\003-pumpkin.png', 1),
(54, '004-mushrooms', 'icon\\大自然可愛圖\\004-mushrooms.png', 1),
(55, '005-acorn', 'icon\\大自然可愛圖\\005-acorn.png', 1),
(56, '006-berry', 'icon\\大自然可愛圖\\006-berry.png', 1),
(57, '007-turkey', 'icon\\大自然可愛圖\\007-turkey.png', 1),
(58, '008-carrot', 'icon\\大自然可愛圖\\008-carrot.png', 1),
(59, '009-owl', 'icon\\大自然可愛圖\\009-owl.png', 1),
(60, '010-corn', 'icon\\大自然可愛圖\\010-corn.png', 1),
(61, '011-squirrel', 'icon\\大自然可愛圖\\011-squirrel.png', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `icon_type`
--

CREATE TABLE `icon_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='icon分類';

--
-- 傾印資料表的資料 `icon_type`
--

INSERT INTO `icon_type` (`type_id`, `type_name`) VALUES
(1, '大自然可愛圖'),
(2, '工具圖示'),
(3, '可愛版'),
(4, '原圖庫'),
(5, '甜點'),
(6, '萬聖節');

-- --------------------------------------------------------

--
-- 資料表結構 `image`
--

CREATE TABLE `image` (
  `img_id` bigint(20) NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `make_user_id` int(11) NOT NULL,
  `info_url` text DEFAULT NULL,
  `pre_img_id` int(11) DEFAULT NULL,
  `pre_do` text DEFAULT NULL,
  `sub_img_id` int(11) DEFAULT NULL,
  `sub_do` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='儲存圖片編輯相關資料';

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`ic_id`),
  ADD KEY `ic_type` (`ic_type`);

--
-- 資料表索引 `icon_type`
--
ALTER TABLE `icon_type`
  ADD PRIMARY KEY (`type_id`);

--
-- 資料表索引 `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `img_do` (`pre_img_id`,`sub_img_id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `icon`
--
ALTER TABLE `icon`
  MODIFY `ic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `icon_type`
--
ALTER TABLE `icon_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `image`
--
ALTER TABLE `image`
  MODIFY `img_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 已傾印資料表的限制式
--

--
-- 資料表的限制式 `icon`
--
ALTER TABLE `icon`
  ADD CONSTRAINT `icon_ibfk_1` FOREIGN KEY (`ic_type`) REFERENCES `icon_type` (`type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
