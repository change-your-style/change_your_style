//JS API
//js_ez_api.js
//Make:2020/03/18
//Author:Ke Fu, Shen
//Version:v1.0.1b

console.info("歡迎使用JS API");

let CONSOLE;
let API_CONSOLE;
let LOG;
let INFO;
let ERROR;

//值表與鍵值表
const CHAR_LIST = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9");
const KEY_LIST = new Array(65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,48,49,50,51,52,53,54,55,56,57);

//產生亂數(最小值,最大值)
function Get_random(min,max){
	var random_int = min+Math.floor(Math.random()*(max-min));
	Api_console("[JS API]: 亂數產生:"+random_int);
	return random_int;
}

//值轉換成鍵值
function Char_to_key(input){
	for(i=0; i<CHAR_LIST.length; i++){
		if(CHAR_LIST[i]==input){
			return KEY_LIST[i];
		}
	}
}

//隨機選擇文字
function char_random() {
	var random_char = CHAR_LIST[Get_random(0,36)];
	Api_console("[JS API]: 亂數文字產生:"+random_char);
	return random_char;
}

function KeyPress(key,fun) {
	$(document).keydown(function (event) {
		if(event.keyCode == key){
			fun = true;
			Api_console(key);
		}
	})
}

//顯示控制台除錯資訊
function Console(type,text){
	if(CONSOLE){
	switch(type){
		case "log":
			if(LOG)
				console.log("["+Time()+"] "+text);
			break;
		case "info":
			if(INFO)
				console.info("["+Time()+"] "+text);
			break;
		case "error":
			if(ERROR)
				console.error("["+Time()+"] "+text);
			break;
		default:
			i = Time();
			console.info("["+i+"] "+"Warring:No set Console type!");
			console.log("["+i+"] "+text);
	}
	}
}

function Api_console(type,text) {
	if(API_CONSOLE){
		Console(type,text);
	}
}

function Time() {
	var dt = new Date;
	return dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
}
