function sobel() {			//炭筆
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, src, cv.COLOR_RGB2GRAY, 0);
	// You can try more different parameters
	cv.Laplacian(src, dst, cv.CV_8U, 1, 20, 0, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();

}

function filter() {			//曝光
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let M = cv.Mat.eye(3, 3, cv.CV_32FC1);
	let anchor = new cv.Point(-1, -1);
	cv.filter2D(src, dst, cv.CV_8U, M, anchor, 0, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function erosion() {		//侵蝕
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let M = cv.Mat.ones(10, 10, cv.CV_8U);
	let anchor = new cv.Point(-1, -1);
	// You can try more different parameters
	cv.dilate(src, dst, M, anchor, 2, cv.BORDER_CONSTANT, cv.morphologyDefaultBorderValue());
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function BLhat() {			//反相
	let src = cv.imread('imageSrc');
	cv.cvtColor(src, src, cv.COLOR_RGBA2RGB);
	let dst = new cv.Mat();
	let M = cv.Mat.ones(53, 53, cv.CV_8U);
	cv.morphologyEx(src, dst, cv.MORPH_BLACKHAT, M);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); M.delete();
}

function code() {			//碼
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let low = new cv.Mat(src.rows, src.cols, src.type(), [0, 0, 0, 0]);
	let high = new cv.Mat(src.rows, src.cols, src.type(), [150, 150, 150, 255]);
	cv.inRange(src, low, high, dst);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete(); low.delete(); high.delete();
}

function gray() {			//灰階
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, dst, cv.COLOR_RGBA2GRAY, 0);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function unclear() {		//模糊
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	let ksize = new cv.Size(3, 10);
	let anchor = new cv.Point(-1, -1);
	// You can try more different parameters
	cv.blur(src, dst, ksize, anchor, cv.BORDER_DEFAULT);
	// cv.boxFilter(src, dst, -1, ksize, anchor, true, cv.BORDER_DEFAULT)
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function smooth() {			//平滑
	let src = cv.imread('imageSrc');
	let dst = new cv.Mat();
	cv.cvtColor(src, src, cv.COLOR_RGBA2RGB, 0);
	cv.bilateralFilter(src, dst, 9, 200, 200, cv.BORDER_DEFAULT);
	cv.imshow('canvasOutput', dst);
	src.delete(); dst.delete();
}

function face(){
	let src = cv.imread('imageSrc');
let gray = new cv.Mat();
cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);
let faces = new cv.RectVector();
let eyes = new cv.RectVector();
let faceCascade = new cv.CascadeClassifier();
let eyeCascade = new cv.CascadeClassifier();
// load pre-trained classifiers
faceCascade.load('haarcascade_frontalface_default.xml');
eyeCascade.load('haarcascade_eye.xml');
// detect faces
let msize = new cv.Size(0, 0);
faceCascade.detectMultiScale(gray, faces, 1.1, 3, 0, msize, msize);
for (let i = 0; i < faces.size(); ++i) {
    let roiGray = gray.roi(faces.get(i));
    let roiSrc = src.roi(faces.get(i));
    let point1 = new cv.Point(faces.get(i).x, faces.get(i).y);
    let point2 = new cv.Point(faces.get(i).x + faces.get(i).width,
                              faces.get(i).y + faces.get(i).height);
    cv.rectangle(src, point1, point2, [255, 0, 0, 255]);
    // detect eyes in face ROI
    eyeCascade.detectMultiScale(roiGray, eyes);
    for (let j = 0; j < eyes.size(); ++j) {
        let point1 = new cv.Point(eyes.get(j).x, eyes.get(j).y);
        let point2 = new cv.Point(eyes.get(j).x + eyes.get(j).width,
                                  eyes.get(j).y + eyes.get(j).height);
        cv.rectangle(roiSrc, point1, point2, [0, 0, 255, 255]);
    }
    roiGray.delete(); roiSrc.delete();
}
cv.imshow('canvasOutput', src);
src.delete(); gray.delete(); faceCascade.delete();
eyeCascade.delete(); faces.delete(); eyes.delete();

}

