//Canvas API
//canvas_ez_api.js
//Make:2020/03/18
//Change Date:2020/12/02
//Author:Ke Fu, Shen
//Version:v1.0.0b
//前置JS:jQuery(jquery-3.4.1.js above)、JS API[v0.0.3](js_ez_api.js)

console.info("歡迎使用Canvas API");

//Canvas物件參數
var CANVAS_ID;
var CANVAS;
var CTX;

var CANVAS_HEIGHT, CANVAS_WIDTH;	//畫布大小

//載入Canvas物件[含物件長寬偵測]
function Canvas_ready(id) {
	Api_console("Canvas API載入中......");
	Canvas_windows(id,0.8,0.8,0,60);	//Default縮放值
	CANVAS_ID = id;
	CANVAS = document.getElementById(CANVAS_ID);
	CTX = canvas.getContext("2d");
	Canvas_size(CANVAS_ID);
	Api_console("Canvas API載入完成!");
}

function Canvas_windows(CANVAS_ID,w_scale,h_scale,w_offset,h_offset){
	 = id;
	document.getElementById(CANVAS_ID).width=document.body.clientWidth * w_scale - w_offset;
	document.getElementById(CANVAS_ID).height=document.body.clientHeight * h_scale - w_offset;
}

//偵測Canvas物件長寬
function Canvas_size(CANVAS_ID) {
	CANVAS_WIDTH = $('#'+CANVAS_ID).attr("width");
	CANVAS_HEIGHT = $('#'+CANVAS_ID).attr("height");
	Api_console("[Canvas API]: 畫布大小:"+CANVAS_WIDTH+"X"+CANVAS_HEIGHT);
}

//Canvas畫圓
//使用方式:
//	canvas_circle(x軸,y軸,半徑,畫圓起始[0度=0,一個圓為1],畫圓結尾,是否填色,填的顏色,是否邊框,邊框顏色,邊框寬度);
function Canvas_circle(x,y,r,start,end,fill,fill_color,stroke,stroke_color,stroke_width){
	CTX.beginPath();
	CTX.arc(x, y, r, 2*Math.PI*start, 2*Math.PI*end);
	CTX.closePath();
	Api_console("[Canvas API]: 畫一顆球 X:"+x+", Y:"+y+", 半徑:"+r+", on "+ canvas +"->ID:"+ CANVAS_ID);
	if(fill==true){
		CTX.fillStyle = fill_color;
		CTX.fill();
		Api_console("\t+填滿:"+fill_color);
	}
	if(stroke==true){
		CTX.strokeStyle = stroke_color;
		CTX.lineWidth = stroke_width;
		CTX.stroke();
		Api_console("\t+邊框:"+stroke_color+","+stroke_width+"px");
	}
}

//Canvas字
//使用方式:
//	canvas_text(x軸,y軸,文字,文字大小,文字顏色);
function Canvas_text(x,y,text,text_font,text_size,text_color){
	CTX.font = text_size+"px " + text_font;
	CTX.fillStyle = text_color;
	CTX.fillText(text, x, y);
	Api_console("[Canvas API]: 寫上文字 文字:"+text+", X:"+x+", Y:"+y+", Size:"+text_size+"px, Color:"+text_color+", on "+ canvas +"->ID:"+ CANVAS_ID);
}

//Canvas 畫布清空
function Canvas_clear() {
	Api_console("[Canvas API]: Canvas Clear!");
	Canvas_size(CANVAS_ID);
	CTX.clearRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);
}

function Canvas_img(){
	src = arguments[0];
	if(arguments.length==3){
		x = arguments[1];
		y = arguments[2];
		CTX.drawImage(src,x,y);
	}else if(arguments.length==5){
		x = arguments[1];
		y = arguments[2];
		width = arguments[3];
		height = arguments[4];
		CTX.drawImage(src,x,y,width,height);
	}else{
		CTX.drawImage(src,0,0);
	}
	
}
