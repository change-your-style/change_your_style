<?php
$target_file = '0000';
$x_file = $target_file . '_1.jpg' ;
$im = imagecreatefromjpeg($target_file . '.jpg');
if($im && imagefilter($im, IMG_FILTER_GAUSSIAN_BLUR))
{
    echo 'Image converted to grayscale.';
    imagepng($im, $x_file);
    echo '<img src= '. $x_file . '>';

}
else
{
    echo 'Conversion to grayscale failed.';
}
imagedestroy($im);
?>